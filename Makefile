# Makefile for the Bank program
CC = g++
CXXFLAGS = -g -Wall -std=c++11
CCLINK = $(CC)
LFLAGS = -g -pthread
OBJS = main.o account.o atm.o bank.o logger.o
RM = rm -f

# Creating the  executable
all: Bank
Bank: $(OBJS)
	$(CCLINK) $(LFLAGS) -o Bank $(OBJS)

# Creating the object files
account.o: account.cc account.h
logger.o: logger.cc logger.h account.h
atm.o: atm.cc atm.h account.h logger.h
bank.o: bank.cc bank.h atm.h account.h logger.h
main.o: main.cc bank.h atm.h account.h logger.h

# Cleaning old files before new make
clean:
	$(RM) $(TARGET) *.o *~ "#"* core.*            